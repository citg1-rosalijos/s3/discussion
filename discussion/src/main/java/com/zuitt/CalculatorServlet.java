package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	private static final long serialVersionUID = 43283931101411089L;
	
	// INITIALIZATION 
	// - first stage of servlet life cycle
	// - init() is used to perform functions such as connecting to database and initializing values
	// - loads the servlet
	// - creates an instance of the servlet class
	// - invokes the "init" method
	public void init() throws ServletException{
		System.out.println("***********************************");
		System.out.println("Initialized connection to database.");
		System.out.println("***********************************");
	}
	
	// SERVICE METHOD (doPost, doPut, doDelete, doGet, service(any type of request)) 
	// - second stage of servlet life cycle that handles the request response
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

		// the parameter names are defined in the form input field.
		// the parameters are found in the url as query string

		int num1 = Integer.parseInt(request.getParameter("num1"));
		int num2 = Integer.parseInt(request.getParameter("num2"));
		int total = num1 + num2;
		
		// the getWriter method is used to print out information in the browser as a response
		PrintWriter out = response.getWriter();
		
		// prints a string in the browser
		// getWriter can also output html elements
		out.println("<h1>Total of the two numbers is " + total + ".</h1>");
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		out.println("<h1>You have accessed the get method of the calculator servlet.</h1>");
	}
	
	// FINALIZATION
	// - last stage of the servlet life cycle
	// - invokes the "destroy" method
	// - cleanup of the resources once the servlet is destroyed or unused
	// - closing the connection
	public void destroy() {
		System.out.println("***************************");
		System.out.println("Disconnected from database.");
		System.out.println("***************************s");
	}
}
